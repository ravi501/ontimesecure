import java.util.Random;


public class OnTimeSecure
{
	public static final char permissions[] = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
	public static final int PERMISSIONS = 8;
	public static final int USER_PERMS = 5;
	
	
	public OnTimeSecure()
	{
		System.out.println("Hello");
	}
	
	public static void main(String[] args)
	{
		int users = 10;
		int roles = 4;
		int role_perm = 3;
		char role[][];
		int role_size[];
		int perm;
		char permis[]; 
		int user_roles[][];
		char req[][];
		Random r = new Random();
		
		
		
		
		
		
		/*Initializing the permissions based on the number of permissions required for the system*/
		permis = new char[PERMISSIONS];
		for(int x = 0; x < PERMISSIONS; x++)
		{
			permis[x] = permissions[x];
		}
		
		/*Allotting Permissions to Roles*/
		role = new char[roles][PERMISSIONS];
		role_size = new int[roles];
		for(int i = 0; i < roles; i++)
		{
			role_size[i] = r.nextInt(PERMISSIONS);
			int j = 0;
			while(j < role_size[i])
			{
				int count = 0;
				perm = r.nextInt(PERMISSIONS);
				System.out.println(perm);
				for(int k = 0; k < j; k++)
				{
					if(j != k)
						if(role[i][k] == permis[perm])
							count = 1;
				}
				if(count == 0)
				{
					role[i][j] = permis[perm];
					j++;
				}
			}
		}
		for(int i = 0; i < PERMISSIONS; i++)
		{
			int val = 0;
			for(int j = 0; j < roles; j++)
			{
				for(int k = 0; k < role_size[j]; k++)
				{
					if(permis[i] == role[j][k])
						val = 1;
				}
			}
			if(val == 0)
			{
				int x = 0;
				while(true)
				{
					if(role_size[x] < PERMISSIONS)
						break;
				}
				role_size[x]++;
				role[i][role_size[x]] = permis[i];
			}
		}
		
		
		/*Users Requirements*/
		req = new char[users][];
		for(int i = 0; i < users; i = i+2)
		{
			int user_perms = r.nextInt(PERMISSIONS);
			int otheruser_perms = USER_PERMS*2 - user_perms;
			
			req[i] = new char[user_perms];
			req[i+1] = new char[otheruser_perms];
			
			int j = 0;
			while(j < user_perms)
			{
				int val = r.nextInt(PERMISSIONS);
				char reqs = permis[val];
				int count = 0;
				for(int k = 0; k < j; k++)
				{
					if(req[i][k] == reqs)
						count = 1;
				}
				if(count == 0)
				{
					req[i][j] = reqs;
					j++;
				}
			}
			j = 0;
			while(j < otheruser_perms)
			{
				int val = r.nextInt(PERMISSIONS);
				char reqs = permis[val];
				int count = 0;
				for(int k = 0; k < j; k++)
				{
					if(req[i+1][k] == reqs)
						count = 1;
				}
				if(count == 0)
				{
					req[i+1][j] = reqs;
					j++;
				}
			}
		}
		
		/*Users to Roles Assignment*/
		user_roles = new int[users][roles];	
		for(int i = 0; i < users; i++)
		{
			char req1[] = req[i];
			
			int role_list[] = new int[roles];
			int role_val;
			int role_count = 0;
			while(true)
			{
				role_val = r.nextInt(roles); 
				int count = 0;
				
				//for(int j = 0; j < )
				
				for(int j = 0; j < req1.length; j++)
				{
					for(int k = 0; k < role[role_val].length; k++)
					{
						if(req1[j] == role[role_val][k])
						{
							count++;
							req1[j] = '0';
							break;
						}
					}
				}
				if(count != 0)
				{	
					user_roles[i][role_count] = role_val;
					role_count++;
				}
				
				int count_elemsatisfied = 0;
				for(int j = 0; j < req1.length; j++)
				{
					if(req1[j] == '0')
						count_elemsatisfied++;
				}
				if(count_elemsatisfied == req1.length)
					break;
			}
		}
		
		
	}
}
