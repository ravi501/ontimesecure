import java.util.HashMap;
import java.util.Random;

/**
 * The AccessControlPerformance program is used for measuring the average case performance of the Role Based Access Control (RBAC) model 
 * 
 * 
 * @author Ravi Akella
 *
 */
public class AccessControlPerformance
{
	/**
	 * Stores the permission set 
	 */
	public static final char permissions[] = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
	
	/**
	 * Stores the total number of permissions in the system
	 */
	int PERMISSIONS;
	
	/**
	 * Stores the total number of permissions a user requires 
	 */
	int USER_PERMS;
	
	/**
	 * Stores the total number of users in the system
	 */
	int users = 10;
	
	/**
	 * Stores the total number of roles in the system
	 */
	int roles;
	
	/**
	 * Stores the average number of permissions in one role
	 */
	int role_perm = 3;
	
	/**
	 * Used for storing the permissions in ith role
	 */
	char role[][];
	
	/**
	 * Stores each role's size
	 */
	int role_size[];
	
	/**
	 * Stores a subset of the above permission set for use in our code. 
	 * This array stores 1st 4 permissions from the above permission set in the 1st iteration.
	 * It stores 1st 8 permissions from the above permission set in the 2nd iteration.
	 */
	char permis[]; 
	
	/**
	 * Stores the roles assigned for ith user in the system
	 */
	int user_roles[][];
	
	/**
	 * Stores the permission requirements for ith user.
	 */
	char req[][];
	
	/**
	 * Stores the permission requirement count for ith user.
	 */
	int req_count[];
	
	/**
	 * Stores the role count assigned to ith user
	 */
	int role_count[];
	
	/**
	 * Stores the user requirement
	 */
	int user_req;
	
	/**
	 * Stores the average number of permissions per role
	 */
	int per_role;
	Random r = new Random();
	
	/**
	 * Function (Constructor): AccessControlPerformance
	 * 
	 * Constructor for initializing the members of the class AccessControlPerformance.   
	 * 
	 */
	public AccessControlPerformance(int users1, int permissions, int user_perms, int noofRoles)
	{
		users = users1;
		roles = noofRoles;
		USER_PERMS = user_perms;
		PERMISSIONS = permissions;
		req_count = new int[users];
		role_count = new int[users];
	}
	
	/**
	 * Function: initializeSystemPermissions()
	 * 
	 * Initializes the total number of permissions in the system
	 * 
	 * If the total number of permission requirement in the system is 4, then permissions a,b,c,d are initialized.
	 * If the total number of permission requirement in the system is 8, then permissions a,b,c,d,e,f,g,h are initialized. 
	 */
	public void initializeSystemPermissions()
	{
		permis = new char[PERMISSIONS];
		for(int i = 0; i < PERMISSIONS; i++)
			permis[i] = permissions[i];
	}
	
	/**
	 * Function: printSystemPermissions()
	 * 
	 * Prints all the permissions currently in the system
	 */
	public void printSystemPermissions()
	{
		System.out.print("The system permissions are: ");
		for(int i = 0; i < PERMISSIONS; i++)
			System.out.print(" " + permis[i]);
		System.out.println();
	}
	
	/**
	 * Function: allotPermissionsToRoles()
	 * 
	 * This function is used for randomly putting permissions into roles depending on the individual role size
	 *
	 * This function makes sure that every permission is present in at least one role and there are no redundant permissions in a single role
	 */
	public void allotPermissionsToRoles()
	{
		per_role = PERMISSIONS/roles;
		role = new char[roles][per_role];
		role_size = new int[roles];
		
		char permis1[] = new char[PERMISSIONS];
		
		for(int i = 0; i < PERMISSIONS; i++)
			permis1[i] = permis[i];
		
		int count1 = 0;
		
		while(count1 < PERMISSIONS)
		{
			int perm;
			char permission;
			int role_val;
			while(true)
			{
				perm = r.nextInt(PERMISSIONS);
				if(permis1[perm] != '1')
				{
					permission = permis1[perm];
					break;
				}
			}
			while(true)
			{
				role_val = r.nextInt(roles);
				if(role_size[role_val] < per_role)
				{
					break;
				}
			}
			role[role_val][role_size[role_val]] = permission;
			permis1[perm] = '1';
			role_size[role_val]++;
			count1++;
		}
	}
	
	/**
	 * Function: printPermissionsToRoles()
	 * 
	 * This function is used for printing all the permissions present in individual roles
	 */
	public void printPermissionsToRoles()
	{
		for(int i = 0; i < roles; i++)
		{
			System.out.print("Role" + i + " size = " + role_size[i] + " permissions: " );
			for(int j = 0; j < role_size[i]; j++)
			{
				System.out.print(role[i][j] + " ");
			}
			System.out.println();
		}
	}
	
	/**
	 * Function: generateUserRequirements()
	 * 
	 * This function generates user requirements randomly based on the number of permissions  user requires
	 */
	public void generateUsersRequirements()
	{
		req = new char[users][USER_PERMS];
		user_req = USER_PERMS;
				
		for(int i = 0; i < users; i++)
		{
			req_count[i] = user_req;
			
			int j = 0;
			while(j < user_req)
			{
				int val = r.nextInt(PERMISSIONS);
				char reqs = permis[val];
				int k = 0;
				while(k < j)
				{
					if(reqs == req[i][k])
					{
						val = r.nextInt(PERMISSIONS);
						reqs = permis[val];
						k = 0;
					}
					else
						k++;
				}
				req[i][j] = reqs;
				j++;
			}
		}
	}
	
	/**
	 * Function: printUsersRequirements()
	 * 
	 * This function prints the users requirements that are generated using generateUserRequirements() function
	 */
	public void printUsersRequirements()
	{
		for(int i = 0; i < users; i++)
		{
			System.out.print("User" + i + " requirements: " );
			for(int j = 0; j < req[i].length; j++)
			{
				System.out.print(req[i][j] + " ");
			}
			System.out.println();
		}
	}
	
	/**
	 * Function: assignRolesToUsers()
	 * 
	 * This function assigns Roles to Users based on Users requirements
	 * This function randomly selects a role and checks if this role satisfies the users requirements. If it satisfies, then the role is assigned to the user.
	 */
	public void assignRolesToUsers()
	{
		user_roles = new int[users][];
				
		for(int i = 0; i < users; i++)
		{
			int req_roles = r.nextInt(roles + 1);
			while(req_roles == 0)
			{
				req_roles = r.nextInt(roles + 1);
			}
			user_roles[i] = new int[roles];
						
			char req1[] = new char[user_req];
			char req2[] = new char[user_req];
			for(int j = 0; j < user_req; j++)
			{
				req1[j] = req[i][j];
				req2[j] = req[i][j];
			}
			
			int role_val;
						
			int count = 0;
			int count10 = 0;
			
			while(count10 < req_roles)
			{
				int count1 = 0;
				role_val = r.nextInt(roles);
				
				for(int k = 0; k < count; k++)
				{
					if(user_roles[i][k] == role_val)
						count1 = 1;
				}
				if(count1 == 0)
				{
					int count4 = 0;
					for(int k = 0; k < user_req; k++)
					{
						for(int l = 0; l < role_size[role_val]; l++)
						{
							if(req1[k] == role[role_val][l])
							{
								if(count4 == 1)
									req2[k] = '0';
								else
								{
									req2[k] = '0';
									count4 = 1;
									user_roles[i][count] = role_val;
									role_count[i]++;
									count++;
								}
								break;
							}
						}
						count10++;
					}
				}
			}
			int count2 = 0;
			for(int j = 0; j < user_req; j++)
			{
				if(req2[j] != '0')
				{
					count2++;
				}
			}
			
			while(count2 > 0)
			{
				int count3 = 0;
				role_val = r.nextInt(roles);
										
				for(int j = 0; j < count; j++)
				{
					if(role_val == user_roles[i][j])
					{
						count3 = 1;
						break;
					}
				}
				if(count3 == 0)
				{
					int count4 = 0;
					for(int k = 0; k < user_req; k++)
					{
						for(int l = 0; l < role_size[role_val]; l++)
						{
							if(req1[k] == role[role_val][l])
							{
								if(count4 == 1)
								{
									req2[k] = '0';
									count2--;
								}
								else
								{
									req2[k] = '0';
									count4 = 1;
									user_roles[i][count] = role_val;
									role_count[i]++;
									count++;
									count2--;
								}
								break;
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * Function: printRolesToUsers()
	 * 
	 * This function prints all the roles assigned to each user
	 */
	public void printRolesToUsers()
	{
		System.out.println("Roles to Users");
		for(int i = 0; i < users; i++)
		{
			System.out.print("User" + i + " :");
			for(int j = 0; j < role_count[i]; j++)
			{
				System.out.print(" " + user_roles[i][j]);
			}
			System.out.println();
		}
	}
	
	/**
	 * Function: calculateOverProvisioning()
	 * 
	 * This function calculates the over-provisioning by subtracting the users permission requirement from permissions assigned to a users fr
	 * 
	 * @return: Returns the average over-provisioning by calculating the total number of over-provisioned permissions in the system
	 */
	public double calculateOverProvisioning()
	{
		double overProvisioning = 0.0;
		for(int i = 0;i < users; i++)
		{
			
			char req1[] = new char[req_count[i]];
			for(int j = 0; j < req_count[i]; j++)
				req1[j] = req[i][j];
					
			int roles1[] = user_roles[i];
			char allotted[] = new char[PERMISSIONS*roles];
			int count = 0;
			int roles;
			
			for(int j = 0; j < role_count[i]; j++)
			{
				
				roles = roles1[j];
				
				for(int k = 0; k < role_size[roles]; k++)
				{
					
					allotted[count] = role[roles][k];
					count++;
				}
			}
			
			
			for(int j = 0; j < count; j++)
			{
				for(int k = j+1; k < count; k++)
					if(allotted[j] == allotted[k])
						allotted[k] = '0';
			}
			int count2 = 0;
		
			for(int j = 0; j < count; j++)
			{	
				if(allotted[j] != '0')
					count2++;
			}
			
			for(int j = 0; j < req_count[i]; j++)
			{
				for(int k = 0; k < count; k++)
				{
					if(req1[j] == allotted[k])
						allotted[k] = '0';
				}
			}
			int count1 = 0;
			
			for(int j = 0; j < count; j++)
			{
				if(allotted[j] != '0')
					count1++;
			}
			
			overProvisioning += (double)count1/count2;
		}
		overProvisioning = overProvisioning/users;
		return overProvisioning;
	}
	
	/**
	 * Function: calculateManageability()
	 * 
	 * This function calculates the manageability by calculating the total number of redundant permissions in the system
	 * 
	 * @return: This function returns the manageability by calculating total number of redundant permissions in the system
	 */
	public double calculateManageability()
	{
		HashMap<Character, Integer> h = new HashMap<Character, Integer>();
		int count;
		for(int i = 0; i < roles; i++)
		{
			for(int j = 0; j < role_size[i]; j++)
			{
				char val = role[i][j];
				if(h.containsKey(val))
				{
					count = h.get(val);
					count++;
					h.put(val, count);
				}
				else
				{
					count = 1;
					h.put(val, count);
				}
			}
		}
		int sum = 0;
		for(int i = 0; i < PERMISSIONS; i++)
		{
			sum = sum + h.get(permis[i]);
		}
		int excessPermissions = sum - PERMISSIONS;
		double redundancy  = (double)excessPermissions/sum;
		double manageability = 1.0 - redundancy;
		return manageability;
	}
	
	/**
	 * Function: main()
	 * 
	 * Used for initializing the other functions and for collecting the data
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		
		int users = 10, permissions = 4, user_perms = 2;
		
		while(permissions <= 24)
		{
			double vulnerability = 0.0;
			double manageability = 0.0;
			int iterations = 100;
			for(int i = 0; i < iterations; i++)
			{
				Random r = new Random();
				
				System.out.println("==============================================");
				System.out.println("Iteration" + i);
				
				//int noofRoles = (int)Math.pow(2, permissions);
				//int roles = r.nextInt(noofRoles);
				
				int roles = 4;
				
				
				
				if(roles == 0)
					roles = 1;
				AccessControlPerformance perf = new AccessControlPerformance(users, permissions, user_perms, roles);
				perf.initializeSystemPermissions();
				
				perf.allotPermissionsToRoles();
				perf.printPermissionsToRoles();
				
				perf.generateUsersRequirements();
				perf.printUsersRequirements();
				
				perf.assignRolesToUsers();
				perf.printRolesToUsers();
				vulnerability += perf.calculateOverProvisioning();
				System.out.println("Vul = " + vulnerability);
				
				manageability += perf.calculateManageability();
			}
			double manageability1 = manageability/iterations;
			double vulnerability1 = vulnerability/iterations;
			System.out.println("Permissions " + permissions + " manageability = " + manageability1);
			System.out.println("Permissions " + permissions + " vulnerability = " + vulnerability1);
			permissions = permissions + 4;
		}
	}
}
